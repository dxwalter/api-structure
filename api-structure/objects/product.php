<?php
class Product{
 
    // database connection and table name
    private $conn;
    private $table_name = "products";
 
    // object properties. table columns
    public $id;
    public $name;
    public $description;
    public $price;
    public $category_id;
    public $category_name;
    public $created;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read products
    public function read(){
    
        // select all query
        $query = "SELECT
                    c.name as category_name, p.id, p.name, p.description, p.price, p.category_id, p.created
                FROM
                    " . $this->table_name . " p
                    LEFT JOIN
                        categories c
                            ON p.category_id = c.id
                ORDER BY
                    p.created DESC";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        return $stmt;
    }

    // create products
    public function create () {
        $query = "INSERT INTO 
                    ". $this -> table_name ."
                SET
                    name = :name, price = :price, description = :description, category_id = :category_id, created = :created
        ";

        $statment = $this -> conn -> prepare($query);

        // sanitize
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->price=htmlspecialchars(strip_tags($this->price));
        $this->description=htmlspecialchars(strip_tags($this->description));
        $this->category_id=htmlspecialchars(strip_tags($this->category_id));
        $this->created=htmlspecialchars(strip_tags($this->created));
    
        // bind values
        $statment->bindParam(":name", $this->name);
        $statment->bindParam(":price", $this->price);
        $statment->bindParam(":description", $this->description);
        $statment->bindParam(":category_id", $this->category_id);
        $statment->bindParam(":created", $this->created);
    
        // execute query
        if($statment->execute()){
            return true;
        }
    
        return false;

    }

}